﻿using UnityEngine;
using WHS;

public class Sample : MonoBehaviour
{
	private CoinAcceptor mCoinAcceptor = null;
	private CoinAcceptor mCoinAcceptor2 = null;

	void Start()
	{
		mCoinAcceptor = new CoinAcceptor(OnHolding, OnAcceptCoin, OnWrongCoin, "COM7");
		mCoinAcceptor.Initialize();
		mCoinAcceptor2 = new CoinAcceptor(OnHolding, OnAcceptCoin, OnWrongCoin, "COM6", OnDataRecieve, OnErrorRecieve);
		mCoinAcceptor2.Initialize();

	}
	public void OnHolding(string iTag)
	{
		Debug.Log(iTag + " OnHolding");
	}
	public void OnAcceptCoin(string iTag)
	{
		Debug.Log(iTag + " OnAcceptCoin");
	}
	public void OnWrongCoin(string iTag)
	{
		Debug.Log(iTag + " OnWrongCoin");
	}
	public void OnDataRecieve(string iTag, string iData)
	{
		Debug.Log(iTag + " " + iData);
	}
	public void OnErrorRecieve(string iTag, string iData)
	{
		Debug.LogError(iTag + " " + iData);
	}
	void OnApplicationQuit()
	{
		mCoinAcceptor?.Close();
		mCoinAcceptor2?.Close();
	}
}
